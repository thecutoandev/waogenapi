﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using AutoMapper;
using Root.Data.Infrastructure;
using Root.Data.Repository;
using Root.Models;
using Website.ViewModels.Common;
using Website.ViewModels.[{{CLASS_NAME}}];

namespace Service.Services
{
    #region Interface
    public interface I[{{CLASS_NAME}}]Service
    {
        [{{CLASS_NAME}}]Datatable Get[{{CLASS_NAME}}]Datatable(int page, int itemsPerPage, string sortBy, bool reverse, string custSearchValue);
        [{{CLASS_NAME}}]ViewModel Get[{{CLASS_NAME}}]Info(int id);
        ResponseStatus Create[{{CLASS_NAME}}]([{{CLASS_NAME}}]ViewModel vm[{{CLASS_NAME}}]);
        ResponseStatus Update[{{CLASS_NAME}}]([{{CLASS_NAME}}]ViewModel vm[{{CLASS_NAME}}]);
        ResponseStatus Delete[{{CLASS_NAME}}](int id);
        void Save[{{CLASS_NAME}}]();
    }
    #endregion

    public class [{{CLASS_NAME}}]Service : I[{{CLASS_NAME}}]Service
    {
        private readonly I[{{CLASS_NAME}}]Repository _[{{CLASS_NAME_VARIABLE}}]Repository;
        private readonly IUnitOfWork _unitOfWork;

        #region Constructor
        public [{{CLASS_NAME}}]Service(I[{{CLASS_NAME}}]Repository [{{CLASS_NAME_VARIABLE}}]Repository, IUnitOfWork unitOfWork)
        {
            this._[{{CLASS_NAME_VARIABLE}}]Repository = [{{CLASS_NAME_VARIABLE}}]Repository;
            this._unitOfWork = unitOfWork;
        }
        #endregion

        #region Get Database

        #region Datatable
        public [{{CLASS_NAME}}]Datatable Get[{{CLASS_NAME}}]Datatable (int page, int itemsPerPage, string sortBy, bool reverse, string searchValue)
        {
            var [{{CLASS_NAME_VARIABLE}}]s = from p in _[{{CLASS_NAME_VARIABLE}}]Repository.GetAllQueryable()
                           select new [{{CLASS_NAME}}]ViewModel()
                           {
                               //[AUTOGEN-SETVALUE]
                           };
            // searching
            if (!string.IsNullOrWhiteSpace(searchValue))
            {
                searchValue = searchValue.ToLower();
				//WAO TODO
                [{{CLASS_NAME_VARIABLE}}]s = [{{CLASS_NAME_VARIABLE}}]s.Where(p => p.Id.ToString().Contains(searchValue));
            }

            var [{{CLASS_NAME_VARIABLE}}]sOrdered = [{{CLASS_NAME_VARIABLE}}]s.OrderBy(sortBy + (reverse ? " descending" : ""));
            List<[{{CLASS_NAME}}]ViewModel> [{{CLASS_NAME_VARIABLE}}]sPaged;
            if(itemsPerPage == -1)
            {
                //if (string.IsNullOrWhiteSpace(searchValue))
                //    return null;
                [{{CLASS_NAME_VARIABLE}}]sPaged = [{{CLASS_NAME_VARIABLE}}]sOrdered.ToList();
            }
            else
            {
                [{{CLASS_NAME_VARIABLE}}]sPaged = [{{CLASS_NAME_VARIABLE}}]sOrdered.Skip((page - 1) * itemsPerPage).Take(itemsPerPage).ToList();
            }
            var [{{CLASS_NAME_VARIABLE}}]sDatatable = new [{{CLASS_NAME}}]Datatable()
            {
                Data = [{{CLASS_NAME_VARIABLE}}]sPaged,
                Total = [{{CLASS_NAME_VARIABLE}}]s.Count()
            };
            return [{{CLASS_NAME_VARIABLE}}]sDatatable;
        }
        #endregion

        #region Info
        public [{{CLASS_NAME}}]ViewModel Get[{{CLASS_NAME}}]Info(int id)
        {
            try
            {
                var [{{CLASS_NAME_VARIABLE}}] = _[{{CLASS_NAME_VARIABLE}}]Repository.Query(p => p.Id == id).FirstOrDefault();
                if ([{{CLASS_NAME_VARIABLE}}] != null)
                {
                    var data = Mapper.Map<[{{CLASS_NAME}}], [{{CLASS_NAME}}]ViewModel>([{{CLASS_NAME_VARIABLE}}]);
                    return data;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }
        #endregion

        #endregion

        #region Update Database

        #region Create
        public ResponseStatus Create[{{CLASS_NAME}}]([{{CLASS_NAME}}]ViewModel vm[{{CLASS_NAME}}])
        {
            try
            {
                var [{{CLASS_NAME_VARIABLE}}] = Mapper.Map<[{{CLASS_NAME}}]ViewModel, [{{CLASS_NAME}}]>(vm[{{CLASS_NAME}}]);
				//WAO TODO START
                [{{CLASS_NAME_VARIABLE}}].CreatedDate = DateTime.Now;
                [{{CLASS_NAME_VARIABLE}}].UpdatedDate = [{{CLASS_NAME_VARIABLE}}].CreatedDate;
				//WAO TODO END
                _[{{CLASS_NAME_VARIABLE}}]Repository.Add([{{CLASS_NAME_VARIABLE}}]);
                Save[{{CLASS_NAME}}]();
                return new ResponseStatus { Successful = true, Message = [{{CLASS_NAME_VARIABLE}}].Id.ToString() };
            }
            catch (Exception ex)
            {
                return new ResponseStatus { Successful = false, Message = ex.Message };
            }
        }
        #endregion

        #region Update
        public ResponseStatus Update[{{CLASS_NAME}}]([{{CLASS_NAME}}]ViewModel vm[{{CLASS_NAME}}])
        {
            try
            {
                var [{{CLASS_NAME_VARIABLE}}] = Mapper.Map<[{{CLASS_NAME}}]ViewModel, [{{CLASS_NAME}}]>(vm[{{CLASS_NAME}}]);
				//WAO TODO START
                [{{CLASS_NAME_VARIABLE}}].UpdatedDate = DateTime.Now;
				//WAO TODO END
                _[{{CLASS_NAME_VARIABLE}}]Repository.Update([{{CLASS_NAME_VARIABLE}}]);
                Save[{{CLASS_NAME}}]();
                return new ResponseStatus { Successful = true, Message = [{{CLASS_NAME_VARIABLE}}].Id.ToString() };
            }
            catch (Exception ex)
            {
                return new ResponseStatus { Successful = false, Message = ex.Message };
            }
        }
        #endregion

        #region Delete
        public ResponseStatus Delete[{{CLASS_NAME}}](int id)
        {
            try
            {
                _[{{CLASS_NAME_VARIABLE}}]Repository.Delete(p => p.Id == id);
                Save[{{CLASS_NAME}}]();
                return new ResponseStatus { Successful = true, Message = id.ToString() };
            }
            catch (Exception ex)
            {
                return new ResponseStatus { Successful = false, Message = ex.Message };
            }
        }
        #endregion

        #endregion

        public void Save[{{CLASS_NAME}}]()
        {
            _unitOfWork.Commit();
        }
    }
}