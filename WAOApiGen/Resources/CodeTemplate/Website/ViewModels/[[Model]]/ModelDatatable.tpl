﻿using System.Collections.Generic;

namespace Website.ViewModels.[{{CLASS_NAME}}]
{
    public class [{{CLASS_NAME}}]Datatable
    {
        public List<[{{CLASS_NAME}}]ViewModel> Data { get; set; }
        public int Total { get; set; }
    }
}