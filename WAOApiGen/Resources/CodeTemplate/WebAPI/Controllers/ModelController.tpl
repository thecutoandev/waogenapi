﻿using System.Web.Http;
using Service.Services;
using Website.ViewModels.[{{CLASS_NAME}}];

namespace WebAPI.Controllers
{
    public class [{{CLASS_NAME}}]Controller:ApiController
    {
        private I[{{CLASS_NAME}}]Service _[{{CLASS_NAME_VARIABLE}}]Service;
        public [{{CLASS_NAME}}]Controller() { }

        #region Constructor
        public [{{CLASS_NAME}}]Controller(I[{{CLASS_NAME}}]Service [{{CLASS_NAME_VARIABLE}}]Service)
        {
            this._[{{CLASS_NAME_VARIABLE}}]Service = [{{CLASS_NAME_VARIABLE}}]Service;
        }
        #endregion

        #region Get Database

        #region Datatable
        [HttpGet]
        [Route("api/[{{CLASS_NAME}}]s/Get[{{CLASS_NAME}}]sForTable")]
        public IHttpActionResult Get(
            int page = 1,
            int itemsPerPage = 10,
            string sortBy = "ID",
            bool reverse = false,
            string search = null
            )
        {
            var [{{CLASS_NAME_VARIABLE}}]Table = _[{{CLASS_NAME_VARIABLE}}]Service.Get[{{CLASS_NAME}}]Datatable(page, itemsPerPage, sortBy, reverse, search);
            if([{{CLASS_NAME_VARIABLE}}]Table == null)
                return NotFound();
            return Ok([{{CLASS_NAME_VARIABLE}}]Table);
        }
        #endregion

        #region Info
        [HttpGet]
        [Route("api/[{{CLASS_NAME}}]s/Get[{{CLASS_NAME}}]Info")]
        public IHttpActionResult Get(int id)
        {
            var [{{CLASS_NAME_VARIABLE}}] = _[{{CLASS_NAME_VARIABLE}}]Service.Get[{{CLASS_NAME}}]Info(id);
            if ([{{CLASS_NAME_VARIABLE}}] == null)
                return NotFound();
            return Ok([{{CLASS_NAME_VARIABLE}}]);
        }
        #endregion

        #endregion

        #region Update Database

        #region Create
        [HttpPost]
        [Route("api/[{{CLASS_NAME}}]s/Create[{{CLASS_NAME}}]")]
        public IHttpActionResult Post([{{CLASS_NAME}}]ViewModel vm[{{CLASS_NAME}}])
        {
            return Ok(_[{{CLASS_NAME_VARIABLE}}]Service.Create[{{CLASS_NAME}}](vm[{{CLASS_NAME}}]));
        }
        #endregion

        #region Update
        [HttpPut]
        [Route("api/[{{CLASS_NAME}}]s/Update[{{CLASS_NAME}}]")]
        public IHttpActionResult Put([{{CLASS_NAME}}]ViewModel vm[{{CLASS_NAME}}])
        {
            return Ok(_[{{CLASS_NAME_VARIABLE}}]Service.Update[{{CLASS_NAME}}](vm[{{CLASS_NAME}}]));
        }
        #endregion

        #region Delete
        [HttpDelete]
        [Route("api/[{{CLASS_NAME}}]s/Delete[{{CLASS_NAME}}]/{id}")]
        public IHttpActionResult Delete(int id)
        {
            return Ok(_[{{CLASS_NAME_VARIABLE}}]Service.Delete[{{CLASS_NAME}}](id));
        }
        #endregion

        #endregion
    }
}