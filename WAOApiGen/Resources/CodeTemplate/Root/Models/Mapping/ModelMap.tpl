﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Root.Models.Mapping
{
    public class [{{CLASS_NAME}}]Map : EntityTypeConfiguration<[{{CLASS_NAME}}]>
    {
        public [{{CLASS_NAME}}]Map()
        {

            #region properties
            //[AUTOGEN-CLASSMAPPING]

            #endregion

            #region Table & Column Mappings
            this.ToTable("[{{CLASS_NAME}}]");
            #endregion
        }
    }
}