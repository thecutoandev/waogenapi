﻿using Root.Data.Infrastructure;
using Root.Models;

namespace Root.Data.Repository
{
	public class [{{CLASS_NAME}}]Repository : RepositoryBase<[{{CLASS_NAME}}]>, I[{{CLASS_NAME}}]Repository
	{
		public [{{CLASS_NAME}}]Repository(IDatabaseFactory databaseFactory)
			: base(databaseFactory)
		{

		}
	}
	public interface I[{{CLASS_NAME}}]Repository : IRepository<[{{CLASS_NAME}}]>
	{
	}
}