﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WAOApiGen.Models
{
    public class RobotTask
    {
        public string MainTask { set; get; }
        public string ContentTask { set; get; }
    }
}
