﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WAOApiGen.Models
{
    public interface ICode
    {
        string ProjectName { set; get; }
        string TemplatePath { set; get; }
        string ClassName { set; get; }
        List<DataField> DataFields { set; get; }
        List<string> ListFile { set; get; }
        string ClassNameVariable { set; get; }
        List<RobotTask> RobotTaks { set; get; }
        void CreateNewFile();
        void InlcudeCode();
    }
}
