﻿using WAOApiGen.Commons;

namespace WAOApiGen.Models
{
    public class DataField
    {
        public string FieldName { set; get; }
        public string DataType {set;get;}
        public bool IsKey { set; get; }
        public int MaxLength { set; get; }
        public bool Required { set; get; }
        public bool Unicode { set; get; }
    }
}
