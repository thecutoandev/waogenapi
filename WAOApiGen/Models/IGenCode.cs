﻿using EnvDTE;

namespace WAOApiGen.Models
{
    public interface IGenCode
    {
        bool DoGenCode(ProjectItem projItem);
        bool IncludeCode(ProjectItem pjItem);
    }
}
