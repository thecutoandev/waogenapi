﻿
namespace WAOApiGen.Models
{
    public interface IWaoProjectItem
    {
        bool CreateNewFile(bool hasSub = true);
        bool IncludeCode();
    }
}
