﻿using System;
using System.IO;
using System.Text;
using EnvDTE;

namespace WAOApiGen.Commons
{
    class LogHelper
    {
        #region Variables
        private const string LOG_FORMAT = ">>>{0}\n>>>>{1}\n>>>>>{2}";
        #endregion

        #region Function
        /// <summary>
        /// Get log path.
        /// </summary>
        /// <param name="dte">DTE object.</param>
        /// <returns></returns>
        public static string GetLogPath(DTE dte)
        {
            try
            {
                //Init and checking exists log folder.
                string logPath = Constant.LOGPATH;
                if (!Directory.Exists(logPath))
                {
                    Directory.CreateDirectory(logPath);
                }
                logPath += string.Format(@"\{0}.log", DateTime.Now.ToString("yyyyMMdd"));
                return logPath;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Write log when exception.
        /// </summary>
        /// <param name="ex">Exception object.</param>
        /// <param name="dte">DTE object.</param>
        public static void WriteLog(Exception ex, DTE dte = null)
        {
            try
            {
                if (dte == null)
                    dte = Constant.GlobalDTE;
                string logPath = GetLogPath(dte);

                if (!string.IsNullOrEmpty(logPath))
                {
                    using (StreamWriter writer = new StreamWriter(new FileStream(logPath, FileMode.Append), new UTF8Encoding(true)))
                    {
                        string log = string.Format(LOG_FORMAT, DateTime.Now.ToString("HH:mm:ss"), ex.Message, ex.StackTrace.Replace("\n", ">>>>>"));
                        writer.WriteLine(log);
                    }
                }
            }
            catch (Exception)
            {
            }
        }
        #endregion
    }
}
