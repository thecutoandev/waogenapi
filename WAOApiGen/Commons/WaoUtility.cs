﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using EnvDTE;
using EnvDTE80;

namespace WAOApiGen.Commons
{
    public static class WaoUtility
    {
        public static IEnumerable<Project> GetAllProjects(this Solution sln)
        {
            return sln.Projects
                .Cast<Project>()
                .SelectMany(GetProjects);
        }

        public static List<ProjectItem> GetAllProjectFolders(this Project prj)
        {
            List<ProjectItem> ret = new List<ProjectItem>();
            if (prj != null)
            {
                var lstFolder = prj.ProjectItems
                                .Cast<ProjectItem>()
                                .Where(x => x != null && x.Kind == EnvDTE.Constants.vsProjectItemKindPhysicalFolder).ToList();
                if (lstFolder.Count > 0)
                {
                    ret.AddRange(lstFolder);
                    foreach (ProjectItem prjItem in lstFolder)
                    {
                        ret.AddRange(GetProjectItems(prjItem));
                    }
                }
            }
            return ret;

        }

        public static IEnumerable<Project> GetProjects(Project project)
        {
            if (project.Kind == ProjectKinds.vsProjectKindSolutionFolder)
            {
                return project.ProjectItems
                    .Cast<ProjectItem>()
                    .Select(x => x.SubProject)
                    .Where(x => x != null)
                    .SelectMany(GetProjects);
            }
            return new[] { project };
        }

        public static List<ProjectItem> GetProjectItems(ProjectItem projectItem)
        {
            List<ProjectItem> ret = new List<ProjectItem>();
            //ret.Add(projectItem);
            if (projectItem != null)
            {
                var lstFolder = projectItem.ProjectItems
                                .Cast<ProjectItem>()
                                .Where(x => x != null && x.Kind == EnvDTE.Constants.vsProjectItemKindPhysicalFolder).ToList();
                if(lstFolder.Count > 0)
                {
                    ret.AddRange(lstFolder);
                    foreach (ProjectItem prjItem in lstFolder)
                    {
                        ret.AddRange(GetProjectItems(prjItem));
                    }
                }
            }
            return ret;
        }

        public static string ConvertToVariableName(string className)
        {
            try
            {
                string returnValue = string.Empty;
                var listChar = className.ToList();
                returnValue = listChar[0].ToString().ToLower();
                for (int index = 1; index < listChar.Count; index++)
                {
                    returnValue += listChar[index].ToString();
                }
                return returnValue;
            }
            catch(Exception ex)
            {
                LogHelper.WriteLog(ex, Constant.GlobalDTE);
                return className;
            }
        }

        public static int FindLineOfText(ProjectItem prjItem, string findText)
        {
            var localPath = prjItem.Properties.Item("LocalPath").Value.ToString();
            int findLine = 0;
            using (StreamReader stream = new StreamReader(localPath))
            {
                string line = string.Empty;
                while ((line = stream.ReadLine()) != null)
                {
                    findLine++;
                    if(line.Trim() == findText)
                    {
                        return findLine;
                    }
                }
            }
            return -1;
        }
    }
}
