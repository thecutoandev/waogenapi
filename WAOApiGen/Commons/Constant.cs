﻿using EnvDTE;
using System.IO;

namespace WAOApiGen.Commons
{
    public class Constant
    {
        public static string RESOURCE_TEMPLATE_PATH_FOLDER = new FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).DirectoryName + @"\Resources\CodeTemplate\{0}";
        public static string LOGPATH = new FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).DirectoryName + @"\Log";
        public static string RESOURCE_PROJECT_LIST = new FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).DirectoryName + @"\Resources\CodeTemplate\ProjectList.txt";
        public static string RESOURCE_PROJECT_ROBOT = new FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).DirectoryName + @"\Resources\CodeTemplate\{0}\robot.txt";
        public static string RootAssemblyPath = new System.IO.FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).DirectoryName;
        public static string TempFolderForCode = new FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).DirectoryName + @"\tmpCode";
        public static DTE GlobalDTE = null;
        public enum DataType
        {
            Int,
            Text,
            DateTime,
            Bit
        }
        public const string ClassNameCSharp = "{0}.cs";
        //Support template
        public const string CreatorTag = @"CREATOR";
        public const string BuilderTag = @"INCLUDE";
        //Support Tag
        public const string ModelBuilderTag = @"[WAO_MODEL-BUILDER]";
        public const string ModelCreatorTag = @"[WAO_MODEL-CREATOR]";
        public const string MappingCreatorTag = @"[WAO_MODEL-CREATOR-MAPPING]";
        public const string RepositoryCreatorTag = @"[WAO_MODEL-CREATOR-REPOSITORY]";
        public const string HasVariableCreatorTag = @"[WAO_CREATOR-HASVARIABLE]";
        public const string HasFolderCreatorTag = @"[WAO_CREATOR-HASFOLDER]";
        public const string CreatorServiceTag = @"[WAO_CREATOR-SERVICE]";
        public const string IncludeTag = @"[WAO_INCLUDE]";
        public const string CreatorFolder = @"[[Model]]";
        public const string LinqTag = @"=>";
        //Other
        public const string TemplateExt = ".tpl";
        public const string CodeExt = ".cs";
        public const string ProjectTag = @"[{{PJ}}{0}]";
        public const string AutoGenMember = @"//[AUTOGEN-MEMBER]";
        public const string RobotTaskFileName = "robot.txt";
        public const string RobotTaskSplit = "-->";
        public const char RobotContentTaskSplit = '|';
        public const string PathSimulator = @"{0}{1}";
        public const char SplitFolder = '\\';

        public class GenCodeType
        {
            public const string ModelName = @"[{{CLASS_NAME}}]";
            public const string ModelVariable = @"[{{CLASS_NAME_VARIABLE}}]";
            public const string ModelMember = @"//[AUTOGEN-MEMBER]";
            public const string ClassMapping = @"//[AUTOGEN-CLASSMAPPING]";
            public const string ClassRepository = @"//[AUTOGEN-REPOSITORY]";
        }

        public class TemPlateCode
        {
            public const string ClassMember = @"public {0} {1} {{set; get;}}";
            public const string HasKey = @"this.HasKey(t => t.{0});";
            public const string PropertyKey = "this.Property(t => t.{0})";
            public const string IsRequired = ".IsRequired()";
            public const string HasMaxLength = ".HasMaxLength(0)";
            public const string IsUnicode = ".IsUnicode({0})";
            public const string HasColumnType =  @".HasColumnType(""{0}"")";
            public const string HasDatabaseGeneratedOption = @".HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)";
        }
    }
}
