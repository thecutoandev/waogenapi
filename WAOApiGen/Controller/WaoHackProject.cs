﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WAOApiGen.Models;
using WAOApiGen.Commons;
using EnvDTE;

namespace WAOApiGen.Controller
{
    public class WaoHackProject : IWaoProject
    {
        //private string[] _listProject;
        private string _className;
        private string _classNameVariable;
        public List<RobotTask> RobotTask;
        private List<DataField> _dataFields;
        private string _projectName;

        public WaoHackProject(string className, List<DataField> dataFields, string projectName)
        {
            //this._listProject = File.ReadAllLines(Constant.RESOURCE_PROJECT_LIST);
            this._className = className;
            this._classNameVariable = WaoUtility.ConvertToVariableName(className);
            this._dataFields = dataFields;
            RobotTask = new List<Models.RobotTask>();
            this._projectName = projectName;
        }

        public bool HackCodeFollowTemplate()
        {
            try
            {
                var project = WaoUtility.GetAllProjects(Constant.GlobalDTE.Solution).FirstOrDefault(p => p.Name == this._projectName);
                if (project == null)
                    return false;
                var projItems = project.ProjectItems
                                       .Cast<ProjectItem>()
                                       .Where(p => p.Kind == Constants.vsProjectItemKindPhysicalFolder)
                                       .ToList();
                if (!GetRobotTask()) return false;
                foreach (RobotTask rbTask in RobotTask)
                {
                    DoHackCode(projItems, project, rbTask, rbTask.MainTask.Contains(Constant.BuilderTag));
                }
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.WriteLog(ex, Constant.GlobalDTE);
                return false;
            }
        }

        private void DoHackCode(List<ProjectItem> projItems, Project project, RobotTask rbTask, bool include = false)
        {
            foreach (ProjectItem pjItem in projItems)
            {
                IWaoProjectItem waoItem = new WaoHackCode(rbTask, pjItem, project, this._className, this._dataFields);
                if (waoItem == null)
                    break;
                switch(include)
                {
                    case true:
                        if (waoItem.IncludeCode())
                            return;
                        break;
                    case false:
                        if (waoItem.CreateNewFile())
                            return;
                        break;
                }
            }
        }

        private bool GetRobotTask()
        {
            try
            {
                RobotTask = new List<RobotTask>();
                var listTask = File.ReadAllLines(string.Format(Constant.RESOURCE_PROJECT_ROBOT, _projectName));
                foreach (string str in listTask)
                {
                    var strSplited = str.Split(new[] { Constant.RobotTaskSplit }, StringSplitOptions.None);
                    if (strSplited.Length > 1)
                    {
                        RobotTask.Add(new RobotTask()
                        {
                            MainTask = strSplited[0],
                            ContentTask = strSplited[1]
                        });
                    }
                }
                return RobotTask.Count == 0 ? false : true;
            }
            catch(Exception ex)
            {
                LogHelper.WriteLog(ex, Constant.GlobalDTE);
                return false;
            }
        }
    }
}
