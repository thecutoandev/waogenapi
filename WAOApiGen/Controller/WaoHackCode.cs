﻿using System;
using System.Collections.Generic;
using System.Linq;
using WAOApiGen.Models;
using WAOApiGen.Commons;
using System.IO;
using EnvDTE;

namespace WAOApiGen.Controller
{
    public class WaoHackCode : IWaoProjectItem
    {
        #region Properties
        public RobotTask RobotTask { set; get; }
        public ProjectItem ProjectItem { set; get; }
        public Project Project { set; get; }
        private string[] _splitedTask;
        private string _className;
        private List<DataField> _dataFields;
        #endregion

        #region Constructor
        public WaoHackCode(RobotTask robotTask, ProjectItem projectItem, Project project, string className, List<DataField> dataFields)
        {
            this.RobotTask = robotTask;
            this.ProjectItem = projectItem;
            this.Project = project;
            this._className = className;
            this._dataFields = dataFields;
            _splitedTask = RobotTask.ContentTask.Split(Constant.RobotContentTaskSplit);
            if (_splitedTask[0].Contains(Constant.CreatorFolder))
            {
                CreateNewFolder();
            }
        }
        #endregion

        #region Create Functions

        public virtual bool CreateNewFolder()
        {
            try
            {
                var indexOfTag = _splitedTask[0].IndexOf(Constant.CreatorFolder);
                var folderPathForCreate = _splitedTask[0].Substring(0, indexOfTag);
                var projectPath = this.Project.Properties.Item("LocalPath").Value.ToString();
                var pathSimulator = string.Format(Constant.PathSimulator, projectPath, folderPathForCreate).TrimEnd(Constant.SplitFolder);
                var pathFolder = this.ProjectItem.Properties.Item("LocalPath").Value.ToString().TrimEnd(Constant.SplitFolder);
                if (pathFolder == pathSimulator)
                {
                    AddFolderToProject(this._className);
                    return true;
                }
                return DirectToChildDoWorkForFolder();
            }
            catch (Exception ex)
            {
                LogHelper.WriteLog(ex, Constant.GlobalDTE);
                return false;
            }
        }

        public virtual bool CreateNewFile(bool hasSub = true)
        {
            try
            {
                var projectPath = this.Project.Properties.Item("LocalPath").Value.ToString();
                var pathSimulator = string.Format(Constant.PathSimulator, projectPath, _splitedTask[0]);
                pathSimulator = new FileInfo(pathSimulator).DirectoryName.Replace(Constant.CreatorFolder, this._className);
                var pathFolder = this.ProjectItem.Properties.Item("LocalPath").Value.ToString().TrimEnd(Constant.SplitFolder);
                if (pathFolder == pathSimulator)
                {
                    AddFileToProject(_splitedTask[0]);
                    return true;
                }
                return DirectToChildDoWorkForFile();
            }
            catch (Exception ex)
            {
                LogHelper.WriteLog(ex, Constant.GlobalDTE);
                return false;
            }
        }

        #endregion

        public virtual bool IncludeCode()
        {
            try
            {
                var projectPath = this.Project.Properties.Item("LocalPath").Value.ToString();
                var pathSimulator = string.Format(Constant.PathSimulator, projectPath, _splitedTask[0]);
                var pathFolder = this.ProjectItem.Properties.Item("LocalPath").Value.ToString().TrimEnd(Constant.SplitFolder);
                if (pathFolder == pathSimulator)
                {
                    WriteCode();
                    return true;
                }
                return DirectToChildDoWorkForInclude();
            }
            catch (Exception ex)
            {
                LogHelper.WriteLog(ex, Constant.GlobalDTE);
                return false;
            }
        }

        #region Add Functions support for create functions

        protected void AddFolderToProject(string folderName)
        {
            var prItem = ProjectItem.ProjectItems.AddFolder(folderName, Constants.vsProjectItemKindPhysicalFolder);
        }

        protected void AddFileToProject(string taskLink)
        {
            var linkTemplate = string.Format(Constant.RESOURCE_TEMPLATE_PATH_FOLDER, this.Project.Name);
            linkTemplate += @"\" + _splitedTask[0];
            var fullPath = new FileInfo(linkTemplate).FullName;
            var prItem = ProjectItem.ProjectItems.AddFromTemplate(fullPath.TrimEnd(char.Parse(@"\")), string.Format(_splitedTask[2], _className));
            IGenCode genCode = GenCodeCreator.Creator(RobotTask.MainTask, _splitedTask[1], this._className, this._dataFields);
            genCode.DoGenCode(prItem);
        }
        #endregion

        #region Write Code Function
        protected void WriteCode()
        {
            IGenCode genCode = GenCodeCreator.Creator(RobotTask.MainTask, _splitedTask[1], this._className, this._dataFields, _splitedTask[2]);
            genCode.IncludeCode(this.ProjectItem);
        }
        #endregion

        #region Direct Functions

        #region For Create File
        protected bool DirectToChildDoWorkForFile()
        {
            var projectItems = this.ProjectItem.ProjectItems
                                               .Cast<ProjectItem>()
                                               .Where(p => p.Kind == Constants.vsProjectItemKindPhysicalFolder)
                                               .ToList();


            foreach (ProjectItem pjItem in projectItems)
            {
                var childItem = new WaoHackCode(this.RobotTask, pjItem, this.Project, this._className, this._dataFields);
                if (childItem.CreateNewFile())
                    return true;
            }
            return false;
        }

        protected bool DirectToChildDoWorkForFolder()
        {
            var projectItems = this.ProjectItem.ProjectItems
                                               .Cast<ProjectItem>()
                                               .Where(p => p.Kind == Constants.vsProjectItemKindPhysicalFolder)
                                               .ToList();


            foreach (ProjectItem pjItem in projectItems)
            {
                var childItem = new WaoHackCode(this.RobotTask, pjItem, this.Project, this._className, this._dataFields);
                if (childItem.CreateNewFolder())
                    return true;
            }
            return false;
        }
        #endregion

        #region For Include
        protected bool DirectToChildDoWorkForInclude()
        {
            var projectItems = this.ProjectItem.ProjectItems
                                              .Cast<ProjectItem>()
                                              .ToList();
            foreach (ProjectItem pjItem in projectItems)
            {
                var childItem = new WaoHackCode(this.RobotTask, pjItem, this.Project, this._className, this._dataFields);
                if (childItem.IncludeCode())
                    return true;
            }
            return false;
        }
        #endregion

        #endregion
    }
}
