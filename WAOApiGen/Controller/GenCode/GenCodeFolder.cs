﻿using System.Collections.Generic;
using WAOApiGen.Models;
using WAOApiGen.Commons;
using EnvDTE;

namespace WAOApiGen.Controller.GenCode
{
    public class GenCodeFolder: GenCodeBase
    {
        public GenCodeFolder(string className, List<DataField> dataFields, string tagForGenCode):base(className, dataFields, tagForGenCode)
        {
            formatCode = string.Empty;
        }
    }
}
