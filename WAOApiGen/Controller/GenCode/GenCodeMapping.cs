﻿using System;
using System.Collections.Generic;
using WAOApiGen.Models;
using WAOApiGen.Commons;
using EnvDTE;

namespace WAOApiGen.Controller.GenCode
{
    public class GenCodeMapping : GenCodeBase
    {
        public GenCodeMapping(string className, List<DataField> dataFields, string tagForGenCode):base(className, dataFields, tagForGenCode)
        {
            formatCode = string.Empty;
        }

        public override void InsertCode(TextSelection sTXT, DataField data)
        {
            string code = string.Empty;
            if(data.IsKey)
            {
                code = string.Format(Constant.TemPlateCode.HasKey, data.FieldName);
                sTXT.Insert(Environment.NewLine + code + ";");
                code = string.Empty;
                code += string.Format(Constant.TemPlateCode.PropertyKey, data.FieldName);
                code += string.Format(Constant.TemPlateCode.IsRequired, true);
                code += string.Format(Constant.TemPlateCode.HasDatabaseGeneratedOption);
                sTXT.Insert(Environment.NewLine + code + ";");
                code = string.Empty;
                return;
            }
            code += string.Format(Constant.TemPlateCode.PropertyKey, data.FieldName);
            if(data.Required)
            {
                code += string.Format(Constant.TemPlateCode.IsRequired, data.Required);
            }
            if (data.Unicode)
            {
                code += string.Format(Constant.TemPlateCode.IsUnicode, data.Unicode);
            }
            if (data.MaxLength > 0)
            {
                code += string.Format(Constant.TemPlateCode.HasMaxLength, data.MaxLength);
            }
            if(data.DataType == "DateTime?")
            {
                code += string.Format(Constant.TemPlateCode.HasColumnType, "datetime2");
            }
            sTXT.Insert(Environment.NewLine + code + ";");
        }
    }
}
