﻿using System;
using System.Collections.Generic;
using WAOApiGen.Models;
using WAOApiGen.Commons;
using EnvDTE;

namespace WAOApiGen.Controller.GenCode
{
    public class GenCodeClassMember : GenCodeBase
    {
        private string _templateCode = string.Empty;
        public GenCodeClassMember(string className, List<DataField> dataFields, string tagForGenCode, string templateCode) : base(className, dataFields, tagForGenCode)
        {
            formatCode = Constant.TemPlateCode.ClassMember;
            _templateCode = templateCode;
        }

        public override bool IncludeCode(ProjectItem prjItem)
        {
            try
            {
                prjItem.Open(Constants.vsViewKindTextView).Activate();
                int findTagCodeLine = WaoUtility.FindLineOfText(prjItem, tagForGenCode);
                if (findTagCodeLine != -1)
                {
                    TextSelection sTXT = (TextSelection)_dte.ActiveDocument.Selection;
                    sTXT.GotoLine(findTagCodeLine);
                    sTXT.EndOfLine();
                    var finalCode = _templateCode.Replace(Constant.GenCodeType.ModelName, this._className);
                    sTXT.Insert(Environment.NewLine + finalCode);
                }
                FormatCode(prjItem);
                return true;
            }
            catch (System.Exception ex)
            {
                LogHelper.WriteLog(ex, Constant.GlobalDTE);
                return false;
            }
        }
    }
}
