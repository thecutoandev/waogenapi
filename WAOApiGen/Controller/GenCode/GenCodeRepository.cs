﻿using System.Collections.Generic;
using WAOApiGen.Models;
using WAOApiGen.Commons;

namespace WAOApiGen.Controller.GenCode
{
    public class GenCodeRepository :GenCodeBase
    {
        public GenCodeRepository(string className, List<DataField> dataFields, string tagForGenCode):base(className, dataFields, tagForGenCode)
        {
            formatCode = string.Empty;
            this.AutoGenClassMember = false;
        }
    }
}
