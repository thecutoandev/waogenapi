﻿using System.Collections.Generic;
using System;
using WAOApiGen.Models;
using WAOApiGen.Commons;
using EnvDTE;

namespace WAOApiGen.Controller.GenCode
{
    public class GenCodeVariable : GenCodeBase
    {
        public GenCodeVariable(string className, List<DataField> dataFields, string tagForGenCode):base(className, dataFields, tagForGenCode)
        {
            formatCode = string.Empty;
            this.AutoGenClassMember = false;
        }
    }
}
