﻿using System.Collections.Generic;
using System;
using WAOApiGen.Models;
using WAOApiGen.Commons;
using EnvDTE;

namespace WAOApiGen.Controller.GenCode
{
    public class GenCodeService: GenCodeBase
    {
        #region Variables
        private string _linq = string.Empty;
        private List<string> _joinedCode = new List<string>();
        #endregion

        public GenCodeService(string className, List<DataField> dataFields, string tagForGenCode):base(className, dataFields, tagForGenCode)
        {
            formatCode = @"{0}={1}.{0}";
            this.AutoGenClassMember = true;
        }

        public override void AutoClassMember(ProjectItem prjItem)
        {
            if (!AutoGenClassMember)
            {
                FormatCode(prjItem);
            }
            string[] splitLinq = new string[] { };
            if(tagForGenCode.Contains(Constant.LinqTag))
            {
                splitLinq = tagForGenCode.Split(new string[] { Constant.LinqTag }, StringSplitOptions.None);
            }
            var tagForSearch = tagForGenCode;
            if (splitLinq.Length >= 2)
            {
                tagForSearch = splitLinq[0];
                _linq = splitLinq[1];
            }
            int findTagCodeLine = WaoUtility.FindLineOfText(prjItem, tagForSearch);
            if (findTagCodeLine != -1)
            {
                TextSelection sTXT = (TextSelection)_dte.ActiveDocument.Selection;
                sTXT.GotoLine(findTagCodeLine);
                sTXT.EndOfLine();
                foreach (DataField data in _dataFields)
                {
                    InsertCode(sTXT, data);
                }
                if(_joinedCode.Count > 0)
                {
                    var finalCode = string.Join(",", _joinedCode);
                    sTXT.Insert(Environment.NewLine + finalCode.Replace(Environment.NewLine, "," + Environment.NewLine));
                }
            }
            FormatCode(prjItem);
        }

        public override void InsertCode(TextSelection sTXT, DataField data)
        {
            string code = string.Format(formatCode, data.FieldName,_linq);
            _joinedCode.Add(code);
        }
    }
}
