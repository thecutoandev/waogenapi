﻿using System;
using System.IO;
using System.Collections.Generic;
using WAOApiGen.Models;
using WAOApiGen.Commons;
using EnvDTE;

namespace WAOApiGen.Controller.GenCode
{
    public abstract class GenCodeBase : IGenCode
    {
        public string tagForGenCode;
        public string formatCode;
        public string _className;
        public List<DataField> _dataFields;
        public DTE _dte;
        public bool AutoGenClassMember;
        public GenCodeBase(string className, List<DataField> dataFields, string tagForGenCode)
        {
            this.tagForGenCode = tagForGenCode;
            this._className = className;
            this._dataFields = dataFields;
            _dte = Constant.GlobalDTE;
            this.AutoGenClassMember = true;
        }
        public virtual bool DoGenCode(ProjectItem projItem)
        {
            try
            {
                projItem.Open(Constants.vsViewKindTextView).Activate();
                TextSelection sTXT = (TextSelection)_dte.ActiveDocument.Selection;
                sTXT.SelectAll();
                sTXT.ReplacePattern(Constant.GenCodeType.ModelName, this._className, (int)vsFindOptions.vsFindOptionsMatchWholeWord);
                sTXT.SelectAll();
                sTXT.ReplacePattern(Constant.GenCodeType.ModelVariable, WaoUtility.ConvertToVariableName(this._className), (int)vsFindOptions.vsFindOptionsMatchWholeWord);
                projItem.Name = projItem.Name.Replace(Constant.TemplateExt, Constant.CodeExt);
                AutoClassMember(projItem);
                return true;
            }
            catch(Exception ex)
            {
                LogHelper.WriteLog(ex, Constant.GlobalDTE);
                return false;
            }
        }

        public virtual void AutoClassMember(ProjectItem prjItem)
        {
            if (!AutoGenClassMember)
            {
                FormatCode(prjItem);
            } 
            int findTagCodeLine = WaoUtility.FindLineOfText(prjItem, tagForGenCode);
            if(findTagCodeLine != -1)
            {
                TextSelection sTXT = (TextSelection)_dte.ActiveDocument.Selection;
                sTXT.GotoLine(findTagCodeLine);
                sTXT.EndOfLine();
                foreach (DataField data in _dataFields)
                {
                    InsertCode(sTXT, data);
                }
            }
            FormatCode(prjItem);
        }

        public virtual void InsertCode(TextSelection sTXT, DataField data)
        {
            string code = string.Format(formatCode, data.DataType, data.FieldName);
            sTXT.Insert(Environment.NewLine + code);
        }

        public virtual bool IncludeCode(ProjectItem pjItem)
        {
            return true;
        }

        public void FormatCode(ProjectItem pjItem)
        {
            try
            {
                _dte.ExecuteCommand("File.SaveAll");
                _dte.ExecuteCommand("File.Close");
                pjItem.Open(Constants.vsViewKindTextView).Activate();
                _dte.ExecuteCommand("Edit.FormatDocument");
                _dte.ExecuteCommand("File.SaveAll");
                _dte.ExecuteCommand("File.Close");
            }
            catch (Exception ex)
            {
                LogHelper.WriteLog(ex, Constant.GlobalDTE);
            }
            
        }
    }
}