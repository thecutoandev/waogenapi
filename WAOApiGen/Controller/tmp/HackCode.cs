﻿using WAOApiGen.Models;
using WAOApiGen.Commons;
using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using EnvDTE;
using EnvDTE80;

namespace WAOApiGen.Controller
{
    public abstract class HackCode : ICode
    {
        #region Porperties
        public string ProjectName { set; get; }
        public string ClassName { set; get; }
        public string TemplatePath { set; get; }
        public List<DataField> DataFields { set; get; }
        #endregion

        #region custom Variables
        public List<string> ListFile { set; get; }
        public string ClassNameVariable { set; get; }
        public List<RobotTask> RobotTaks { set; get; }
        private string _robotLink;
        public List<ProjectItem> Folders { set; get; }
        #endregion

        #region Constructor
        public HackCode(string className, List<DataField> dataFields)
        {
            this.ClassName = className;
            this.ClassNameVariable = ConvertToVariableName(ClassName);
            this.ProjectName = string.Empty;
            this.TemplatePath = string.Format(Constant.RESOURCE_TEMPLATE_PATH_FOLDER, this.ProjectName);
            this.DataFields = dataFields;
        }
        #endregion

        #region Overide functions
        public virtual void CreateNewFile()
        {
            ListFile = new List<string>();
            GetAllFiles(this.TemplatePath);
            if (!GetRobotTask())
            {
                return;
            }
        }
        public virtual void InlcudeCode()
        {
            
        }
        #endregion

        #region Private function
        private void GetAllFiles(string rootFolder, bool isRoot = true)
        {
            if(isRoot)
            {
                var lst = Directory.GetFiles(rootFolder).ToList();
                _robotLink = string.Empty;
                if (lst != null)
                {
                    ListFile.AddRange(lst);
                    _robotLink = lst.FirstOrDefault(l => l.Contains(Constant.RobotTaskFileName));
                }
            }
            foreach (string folder in Directory.GetDirectories(rootFolder))
            {
                GetAllFiles(folder, false);
            }
        }

        private string ConvertToVariableName(string className)
        {
            try
            {
                string returnValue = string.Empty;
                var listChar = className.ToList();
                returnValue = listChar[0].ToString().ToLower();
                for (int index = 1; index < listChar.Count; index++)
                {
                    returnValue += listChar[index].ToString();
                }
                return returnValue;
            }
            catch(Exception ex)
            {
                LogHelper.WriteLog(ex, Constant.GlobalDTE);
                return className;
            }
        }
        private bool GetRobotTask()
        {
            try
            {
                RobotTaks = new List<RobotTask>();
                var listTask = File.ReadAllLines(_robotLink);
                foreach(string str in listTask)
                {
                    var strSplited = str.Split( new[] { Constant.RobotTaskSplit }, StringSplitOptions.None);
                    if(strSplited.Length > 1)
                    {
                        RobotTaks.Add(new RobotTask()
                        {
                            MainTask = strSplited[0],
                            ContentTask = strSplited[1]
                        });
                    }
                }
                return true;
            }
            catch(Exception ex)
            {
                LogHelper.WriteLog(ex, Constant.GlobalDTE);
                return false;
            }
        }
        #endregion
    }
}
