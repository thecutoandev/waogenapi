﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WAOApiGen.Models;

namespace WAOApiGen.Controller
{
    public class HackCodeCreator
    {
        public static ICode CreateHackCode(string projectName, string className, List<DataField> dataFields)
        {
            try
            {
                switch (projectName)
                {
                    case "Root": return new RootProject(className, dataFields);
                    default:
                        return null;
                }
            }
            catch
            {
                return null;
            }
        }
    }
}
