﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WAOApiGen.Models;
using WAOApiGen.Commons;
using EnvDTE;
using EnvDTE80;

namespace WAOApiGen.Controller
{
    public class RootProject : HackCode
    {
        public RootProject(string className, List<DataField> dataFields):base(className, dataFields)
        {
            this.ProjectName = "Root";
            this.TemplatePath = string.Format(Constant.RESOURCE_TEMPLATE_PATH_FOLDER, this.ProjectName);
        }

        public override void CreateNewFile()
        {
            base.CreateNewFile();
            var project = WaoUtility.GetAllProjects(Constant.GlobalDTE.Solution).FirstOrDefault(p => p.Name == this.ProjectName);
            if (project == null)
                return;
            Folders = WaoUtility.GetAllProjectFolders(project);
            var waoCreator = this.RobotTaks.Where(r => r.MainTask == Constant.ModelCreatorTag);
            foreach(RobotTask rb in waoCreator)
            {
                CreatorClass(rb, project);
            }
        }

        private void CreatorClass(RobotTask robotTask, Project project)
        {
            var splitTask = robotTask.ContentTask.Split(Constant.RobotContentTaskSplit);
            var projectPath = project.Properties.Item("LocalPath").Value.ToString();
            var pathSimulator = string.Format(Constant.PathSimulator, projectPath, splitTask[0]);
            pathSimulator = new FileInfo(pathSimulator).DirectoryName;
            foreach (ProjectItem item in Folders)
            {
                var pathFolder = item.Properties.Item("LocalPath").Value.ToString().TrimEnd(Constant.SplitFolder);
                if(pathFolder == pathSimulator)
                {
                    var linkTemplate = string.Format(Constant.RESOURCE_TEMPLATE_PATH_FOLDER, this.ProjectName);
                    linkTemplate += @"\" + splitTask[0];
                    var fullPath = new FileInfo(linkTemplate).FullName;
                    GenFile(item.ProjectItems.AddFromFile(fullPath));
                    return;
                }
            }
        }

        private bool GenFile(ProjectItem prjItem)
        {
            prjItem.Open(Constants.vsViewKindCode);
            return true;
        }
    }
}
