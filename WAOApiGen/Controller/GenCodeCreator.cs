﻿using System.Collections.Generic;
using WAOApiGen.Models;
using WAOApiGen.Commons;
using WAOApiGen.Controller.GenCode;

namespace WAOApiGen.Controller
{
    public class GenCodeCreator
    {
        public static IGenCode Creator(string tagForDetectType, string tagForGenCode, string className, List<DataField> dataFields, string templateCode = "")
        {
            switch (tagForDetectType)
            {
                case Constant.ModelCreatorTag:
                case Constant.IncludeTag:
                    return new GenCodeClassMember(className, dataFields, tagForGenCode, templateCode);
                case Constant.MappingCreatorTag: return new GenCodeMapping(className, dataFields, tagForGenCode);
                case Constant.RepositoryCreatorTag: return new GenCodeRepository(className, dataFields, tagForGenCode);
                case Constant.HasVariableCreatorTag:
                    return new GenCodeVariable(className, dataFields, tagForGenCode) { AutoGenClassMember = true};
                case Constant.HasFolderCreatorTag:
                    return new GenCodeVariable(className, dataFields, tagForGenCode);
                case Constant.CreatorServiceTag: return new GenCodeService(className, dataFields, tagForGenCode);
            }
            return null;
        }
    }
}
