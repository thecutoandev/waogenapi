﻿namespace WAOApiGen.Forms
{
    partial class WAOHackCode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtTableName = new MetroFramework.Controls.MetroTextBox();
            this.grdMain = new System.Windows.Forms.DataGridView();
            this.clnmNameField = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clnmDatatype = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.clnmIdentity = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.clnmMaxLength = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clnmRequired = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.clnmUnicode = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.btnCreateAPI = new MetroFramework.Controls.MetroButton();
            this.metroPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMain)).BeginInit();
            this.SuspendLayout();
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.metroLabel1);
            this.metroPanel1.Controls.Add(this.txtTableName);
            this.metroPanel1.Controls.Add(this.grdMain);
            this.metroPanel1.Controls.Add(this.btnCreateAPI);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(20, 60);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(660, 420);
            this.metroPanel1.TabIndex = 0;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(3, 7);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(79, 19);
            this.metroLabel1.TabIndex = 5;
            this.metroLabel1.Text = "Table Name";
            // 
            // txtTableName
            // 
            this.txtTableName.Location = new System.Drawing.Point(88, 7);
            this.txtTableName.Name = "txtTableName";
            this.txtTableName.Size = new System.Drawing.Size(569, 23);
            this.txtTableName.TabIndex = 4;
            // 
            // grdMain
            // 
            this.grdMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdMain.BackgroundColor = System.Drawing.Color.White;
            this.grdMain.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdMain.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clnmNameField,
            this.clnmDatatype,
            this.clnmIdentity,
            this.clnmMaxLength,
            this.clnmRequired,
            this.clnmUnicode});
            this.grdMain.GridColor = System.Drawing.SystemColors.ActiveCaption;
            this.grdMain.Location = new System.Drawing.Point(3, 41);
            this.grdMain.Name = "grdMain";
            this.grdMain.Size = new System.Drawing.Size(656, 346);
            this.grdMain.TabIndex = 3;
            // 
            // clnmNameField
            // 
            this.clnmNameField.HeaderText = "Field Name";
            this.clnmNameField.Name = "clnmNameField";
            this.clnmNameField.Width = 240;
            // 
            // clnmDatatype
            // 
            this.clnmDatatype.HeaderText = "Data Type";
            this.clnmDatatype.Items.AddRange(new object[] {
            "int",
            "string",
            "DateTime?",
            "bool",
            "decimal"});
            this.clnmDatatype.Name = "clnmDatatype";
            this.clnmDatatype.Width = 120;
            // 
            // clnmIdentity
            // 
            this.clnmIdentity.HeaderText = "Key";
            this.clnmIdentity.Name = "clnmIdentity";
            this.clnmIdentity.Width = 103;
            // 
            // clnmMaxLength
            // 
            this.clnmMaxLength.HeaderText = "Max Length";
            this.clnmMaxLength.Name = "clnmMaxLength";
            this.clnmMaxLength.Width = 50;
            // 
            // clnmRequired
            // 
            this.clnmRequired.HeaderText = "Not Null";
            this.clnmRequired.Name = "clnmRequired";
            this.clnmRequired.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.clnmRequired.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.clnmRequired.Width = 50;
            // 
            // clnmUnicode
            // 
            this.clnmUnicode.HeaderText = "Unicode";
            this.clnmUnicode.Name = "clnmUnicode";
            this.clnmUnicode.Width = 50;
            // 
            // btnCreateAPI
            // 
            this.btnCreateAPI.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCreateAPI.Location = new System.Drawing.Point(537, 393);
            this.btnCreateAPI.Name = "btnCreateAPI";
            this.btnCreateAPI.Size = new System.Drawing.Size(122, 23);
            this.btnCreateAPI.TabIndex = 2;
            this.btnCreateAPI.Text = "Hack Code API";
            this.btnCreateAPI.Click += new System.EventHandler(this.btnCreateAPI_Click);
            // 
            // WAOHackCode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = MetroFramework.Drawing.MetroBorderStyle.FixedSingle;
            this.ClientSize = new System.Drawing.Size(700, 500);
            this.Controls.Add(this.metroPanel1);
            this.Name = "WAOHackCode";
            this.Resizable = false;
            this.Text = "WAO Hack code";
            this.Load += new System.EventHandler(this.WAOHackCode_Load);
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMain)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroButton btnCreateAPI;
        private System.Windows.Forms.DataGridView grdMain;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox txtTableName;
        private System.Windows.Forms.DataGridViewTextBoxColumn clnmNameField;
        private System.Windows.Forms.DataGridViewComboBoxColumn clnmDatatype;
        private System.Windows.Forms.DataGridViewCheckBoxColumn clnmIdentity;
        private System.Windows.Forms.DataGridViewTextBoxColumn clnmMaxLength;
        private System.Windows.Forms.DataGridViewCheckBoxColumn clnmRequired;
        private System.Windows.Forms.DataGridViewCheckBoxColumn clnmUnicode;
    }
}