﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MetroFramework.Forms;
using System.Windows.Forms;
using WAOApiGen.Models;
using WAOApiGen.Commons;
using WAOApiGen.Controller;

namespace WAOApiGen.Forms
{
    public partial class WAOHackCode : MetroFramework.Forms.MetroForm
    {
        #region Properties
        private List<DataField> _dataFields;
        #endregion

        public WAOHackCode()
        {
            InitializeComponent();
            _dataFields = new List<DataField>();
        }

        private void btnCreateAPI_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            this.Hide();
            InitListDataFields();
            string[] projectList = File.ReadAllLines(Constant.RESOURCE_PROJECT_LIST);
            LogHelper.WriteLog(new Exception(string.Format("Get Project List Ok --> {0}",projectList)), Constant.GlobalDTE);
            foreach (string prjName in projectList)
            {
                IWaoProject waoProject = new WaoHackProject(this.txtTableName.Text, _dataFields, prjName);
                waoProject.HackCodeFollowTemplate();
            }
            MessageBox.Show("Done. But I unknow success or error :))");
            this.Close();
        }

        private bool InitListDataFields()
        {
            DataGridViewRow drwError = null;
            try
            {
                _dataFields.Clear();
                if (grdMain.Rows.Count <= 1)
                {
                    return false;
                }
                for (int index = 0; index < grdMain.Rows.Count; index++)
                {
                    DataGridViewRow drw = grdMain.Rows[index];
                    if (drw.Cells[clnmNameField.Name].Value == null)
                    {
                        drwError = drw;
                        throw new Exception("Name must not empty.");
                    }
                    if (!CheckExistsFieldNameInList(drw.Cells[clnmNameField.Name].Value.ToString()))
                    {
                        DataGridViewComboBoxCell dtCell = drw.Cells[clnmDatatype.Name] as DataGridViewComboBoxCell;
                        DataGridViewCheckBoxCell dtChkCell = drw.Cells[clnmIdentity.Name] as DataGridViewCheckBoxCell;
                        DataGridViewCheckBoxCell dtChkCellNotNull = drw.Cells[clnmRequired.Name] as DataGridViewCheckBoxCell;
                        DataGridViewCheckBoxCell dtChkCellUnicode = drw.Cells[clnmUnicode.Name] as DataGridViewCheckBoxCell;
                        _dataFields.Add(new DataField()
                        {
                            FieldName = drw.Cells[clnmNameField.Name].Value.ToString(),
                            DataType = dtCell.Value.ToString(),
                            IsKey = (bool)(dtChkCell.Value == null ? false : dtChkCell.Value),
                            MaxLength = int.Parse((drw.Cells[clnmMaxLength.Name]).Value.ToString()),
                            Required = (bool)(dtChkCellNotNull.Value == null ? false : dtChkCellNotNull.Value),
                            Unicode = (bool)(dtChkCellUnicode.Value == null ? false : dtChkCellUnicode.Value)
                        });
                    }
                    else
                    {
                        drwError = drw;
                        throw new Exception("Field name is duplicate");
                        //drw.Cells[clnmNameField.Name].ErrorText = "Field name is duplicate";
                        //return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                drwError.Cells[clnmNameField.Name].ErrorText = ex.Message;
                LogHelper.WriteLog(ex, Constant.GlobalDTE);
                return false;
            }

        }

        private bool CheckExistsFieldNameInList(string fieldName)
        {
            try
            {
                var field = _dataFields.FirstOrDefault(d => d.FieldName == fieldName);
                if (field != null)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        private void WAOHackCode_Load(object sender, EventArgs e)
        {
            //MessageBox.Show(Constant.RESOURCE_TEMPLATE_PATH_FOLDER);
            //MessageBox.Show(Constant.LOGPATH);
            //this.txtTableName.Text = @"BugTracker";
            //this.grdMain.Rows.Add("Id", "int", true, 0, true);
            //this.grdMain.Rows.Add("Text", "string", false, 1000, true);
            //this.grdMain.Rows.Add("DebugDate", "DateTime?", false, 0, false);
            //this.grdMain.Rows.Add("DebugBit", "bool", false, 0, true);
            //this.grdMain.Rows.Add("DebugDecimal", "decimal", false, 0, true);
        }
    }
}
